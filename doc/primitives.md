# Primitives

## Integral Types

**Integral types** should be used from the [\<cstdint\>](https://en.cppreference.com/w/cpp/header/cstdint)
header, those which consist of:

* [u] int8_t
* [u] int16_t
* [u] int32_t
* [u] int64_t

### Examples:

```cpp
#include <cstdint>

int main(int argc, char const* argv[])
{
  uint16_t counter;
  uint64_t grid_size;
  return EXIT_SUCCESS;
}
```

## Floating-Point Types:

**Floating-Point Types** should the alises below:

* float32_t
* float64_t

### Examples

```cpp
typedef float float32_t;
typedef double float64_t;

int main(int argc, char const* argv[])
{
  float32_t pi{3.14};
  float64_t res{8.77443};
  return EXIT_SUCCESS;
}
```

## References

[cstdint](http://www.cplusplus.com/reference/cstdint/)

[fundamental-types](https://en.cppreference.com/w/cpp/language/types)
