# CRUD

**CRUD** stands for **Create Read Update and Delete**. These operations are
commonly used for a variety of applications in C++ classes. Here
are defined the conventions to a more uniform methodology of naming
the methods of your classes.

Consider the example below:

```cpp
#include <iostream>
#include <memory>
#include <utility>
#include <vector>

namespace hohenheim
{

template<typename T>
using Data = std::vector<T>;

template<typename T>
using const_iterator = typename Data<T>::const_iterator;

template<typename T>
using Storage = std::shared_ptr<Data<T>>;

template<typename T>
class MyClass
{
  private:
  // Private members
    Storage<T> data;
  public:
  // Constructors
    template<typename U>
    MyClass(U&& u) noexcept;
  // Iterators
    const_iterator<T> cbegin() const noexcept;
    const_iterator<T> cend() const noexcept;
  // Public Methods
    // Modifiers
    T const& at() const noexcept;
    template<typename U>
    void push_back(U&& u) noexcept;
    template<typename RandIt>
    void erase(RandIt&& it) noexcept;
};

} // namespace hohenheim
```

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> For your class methods, use names in accordance with the standard
> library, in this example, the operations used were base on the
> ones implemented for the
> [std::vector](https://en.cppreference.com/w/cpp/container/vector)
> container.

> <i class="fa fa-question-circle" style="color:seagreen"></i>&nbsp;&nbsp;
> How will this help me in the long run?<br><br>
> Consider that the software you are developing is composed of a large amount
> of classes; for each of them, you define distinct names for CRUD operations,
> for example, remove for one, delete for the other, and erase for a third one.
> You will have to constantly check the class file or the documentation to know
> which method is the one you are looking for. Choosing erase for all makes it
> transparent what you should write to perform the action of erasing an item.
