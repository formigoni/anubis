# Storages

Inspired by the
[EPFL Logic Synthesis Libraries](https://msoeken.github.io/index.html)
it is recommended to use a **Storage** alias to instantiate a data item inside
your class.

## How does it work?

Taking advantage of `using` declarations you can increase the readability
of your code with simple aliases, below is sample snippet of a generic
graph class using this convention:

```cpp
namespace graph {

// First alias to define nodes
template<typename T>
using Nodes = std::multimap<T,T>;
// Second one to put them into dynamic memory
template<typename T>
using Storage = std::unique_ptr<Nodes<T>>;

template<typename T>
class Graph
{
  private:
  // Private Members
    Storage<T> graph; // Simple and concise.
  public:
  // Constructors
    Graph();
    Graph(std::initializer_list<std::pair<T,T>> t);
  // Public Methods
    // Element Access
    template<typename U>
    std::vector<T> get_adjacent(U&& u);
    // Modifiers
    template<typename U>
    void emplace(U&& u);
    template<typename U, typename... Args>
    void emplace(U&& u, Args&&... args);
};

} // namespace graph
```

> <i class="fa fa-question-circle" style="color:seagreen"></i>&nbsp;&nbsp;
> What if I have more than one data item in my class?<br>
> Replace storage with meaningful names, preferably generic
> if using templates. Given a generic `Person` class:
> ```cpp
> ...
>  Name<T> name; // Could be a struct with first, middle, last; or a string
>  Address<T> address; // Could be a struct with the coherent fields.
> ...
> ```
