# Directories

## Naming

Files and directories should be named using
lower-case words separated using a dash '-'.

This rule does not apply to a *CMakeLists* file.

### Examples:

```text
- my-project
| - include
| | - CMakeLists.txt
| | - heianhouer
| | | - heianhouer.hpp
| - extern
| - doc
| - CMakeLists.txt
```

## Recommended Project Structure

Given a project named *anastasia*, the recommended project
structure is shown below:

```text
- anastasia
| - include
| | - CMakeLists.txt
| | - anastasia
| | | - component-a.hpp
| | | - component-a
| | | | - algorithm.hpp
| | | | - enumeration.hpp
| | | - component-b.hpp
| | | - component-b
| | | | - binary-tree.hpp
| | | | - data-structure.hpp
| - extern
| - doc
| - test
| - CMakeLists.txt
```

Where:

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> The root folder has to match the project's name.

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> An include folder has **all** the source files.

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> Each component has a header to its functionalities.<br><br>
> Each of these functionalities, those needing algorithms or data structures,
> are stored in a directory with the same name of the component header.

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> An extern folder **must** contain **all** the external libraries.

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> A test folder, should be used for testing modules and algorithms.<br><br>
> <i class="fa fa-exclamation-triangle" style="color:darkkhaki"></i>&nbsp;&nbsp;
> The **test** folder, should always, be used in the singular.

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> A doc folder is used for all the project's documentation.
