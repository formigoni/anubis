# Class

Templated Class:

```cpp
template<typename T>
class Storage
{
  private:
  // Private members
  public:
  // Public Members
  public:
  // Constructors
  // Iterators
  // Public Methods
    // Element Access
    // Capacity
    // Modifiers
    // Lookup
    // Operations
    // Observers
  private:
  // Private Methods
};
```

Non-templated Class

```cpp
class Storage
{
  private:
  // Private members
  public:
  // Public Members
  public:
  // Constructors
  // Iterators
  // Public Methods
    // Element Access
    // Capacity
    // Modifiers
    // Lookup
    // Operations
    // Observers
  private:
  // Private Methods
};
```
