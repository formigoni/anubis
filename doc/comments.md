# Comments

## Inline comments

Should be of the form **//**.

### Example:
```cpp
uint32_t work_hours; // Includes extra hours
```

## Multiline comments

Should be of the same form mentioned before, expanded throughout
as many lines as necessary.

### Example:
```cpp
//
// Quick-sort implementation.
// Uses iterators from the begining and end
// of the container.
//
```

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> Notice the leading and the trailing **//** above, its recommended to use
them so they better separate the code from your comment.

> <i class="fa fa-exclamation-triangle" style="color:darkkhaki"></i>&nbsp;&nbsp;
> **DO NOT**, use **/*** ***/**, inline or multiline comments.
