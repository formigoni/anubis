# Enumerations

**Enumerations** definitions are written in 
[CamelCase](https://pt.wikipedia.org/wiki/CamelCase).
Preferably, use
[scoped enumerations](https://en.cppreference.com/w/cpp/language/enum),
below is an adapted example from the
[cppreference](https://en.cppreference.com/w/cpp/language/enum)
website.

### Example

```cpp
enum class Altitude: char
{ 
  HIGH='h',
  LOW='l', // C++11 allows the extra comma
}; 
```

The enumerators names are written in **UPPERCASE**.

### Example

```cpp
enum class Direction
{
  LEFT,
  RIGHT,
  UP,
  DOWN,
};
```
