# Summary

- [Good Practices](./good-practices.md)
  - [CRUD](./good-practices/crud.md)
  - [East Const](./good-practices/const.md)
  - [Storages](./good-practices/storages.md)
- [Comments](./comments.md)
- [Namespaces](./namespaces.md)
- [Scopes](./scopes.md)
- [Primitives](./primitives.md)
- [Identifiers](./identifiers.md)
- [Enumerations](./enumerations.md)
- [Classes](./classes.md)
- [Templates](./templates.md)
- [Concepts](./concepts.md)
- [Iterators](./iterators.md)
- [Directories](./directories.md)
- [Resources](./resources.md)
  - [Class](./resources/class.md)
