# Identifiers

Identifiers should be written in
[snake_case](https://en.wikipedia.org/wiki/Snake_case).

### Examples:

```cpp
int32_t my_variable{};
uint64_t total_cost{};
float64_t score{};
```
