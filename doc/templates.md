# Templates

This section describes the used conventions when using templates with
classes, algorithms and iterators.

## Classes and Functions

### Templated classes and functions

Identifiers should be written in
[CamelCase](https://pt.wikipedia.org/wiki/CamelCase).

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> For one template parameter use `T` or a descriptive name, e,g:.<br>
```cpp
template<typename T>
T sum( T&& a, T&& b )
{
  return a+b;
}
```
or
```cpp
template<typename Integral>
T sum( Integral&& a, Integral&& b )
{
  return a+b;
}
```

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> For two template parameters, use `T` and `U`, or a descriptive name.<br>
```cpp
template<typename T, typename U>
auto sum_first( T&& a, U&& b )
{
  return a.first+b.first;
}
```
or
```cpp
template<typename Pair1, typename Pair2>
auto sum_first( Pair1&& a, Pair2&& b )
{
  return a.first+b.first;
}
```

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> For more than 2 template parameters, use `T1 ... Tn` or a descriptive name.<br>
```cpp
template<typename T1, typename T2, typename T3>
bool all_equal( T1&& a, T2&& b, T3&& c)
{
  return (a == b) && (b == c);
}
```
