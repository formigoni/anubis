# East `const`

## The `const` Keyword

Follow the **east const** convention, which be better explained by an example,
lets start with the `int32_t` and `char` keywords.

```cpp
int32_t const var{};           // East const
const int32_t var{};           // West const
char const * string {"Hello"}; // East const
const char * string {"Hello"}; // West const
```

The first and second lines can be read, from the right to the left
as follows:
> An identifier to a constant integer of 32 bits.
> 
> A identifier to a pointer to a constant char

Taking it a step further, it is possible to see the consistency
when reading code using convention, consider the snippet below:

```cpp
char const * const string {"Hello"};
```

The from the right to the left it reads as follows:
> An identifier to a constant pointer to a constant char.

## The `constexpr` Keyword
For the `constexpr` keyword, keep it always in the `begging` of the statement.

```cpp
constexpr int32_t const   var       {42};
constexpr char    const * my_string {"Hello"};
```

To put it into words, the first and second lines would translate to:
> A identifier that is a constant integer of 32 bits.
> 
> A identifier that is a pointer to a constant char.

> <i class="fa fa-question-circle" style="color:seagreen"></i>&nbsp;&nbsp;
> `constexpr const` &nbsp; Some compilers will give a warning if you just specify
> `constexpr` when making a char\* constant.

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> If you omit the `const` keyword, use the east `const` rule on
> `constexpr`:
> ```cpp
> constexpr int32_t var {42};
> ```

## The `static` keyword

For the `static` keyword, use it always at the **beggining** of
your variable definition or declaration ( in the case where
the definition of the static variable is elsewhere  ).

```cpp
static constexpr int32_t const         var       {42};
static constexpr char    const * const my_string {"Hello"};
```

The lines in the snippet above can be read as follows:

> An identifier to a constant integer of 32 bits which is static.
>
> An identifier to a constant pointer to a constant char which is static.
