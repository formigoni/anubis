# Classes

## Naming

Class names are written in [CamelCase](https://pt.wikipedia.org/wiki/CamelCase).

### Examples:

```cpp
class CamelCase
{
  MyClass() = default;
}
```

```cpp
class MyInterface
{
  insert() = 0;
}
```

## Member functions

The member functions of a class are written in
[snake_case](https://en.wikipedia.org/wiki/Snake_case).

**Friends** and **operators** of a class are also included.

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> For the naming of your classes methods, choose names based on
> [containers](https://en.cppreference.com/w/cpp/container)
> or 
> [algorithms](https://en.cppreference.com/w/cpp/algorithm)
> from the C++ standard library, this way, it is
> easier to remember the name of a method to execute an action,
> since all follow the same pattern.

> <i class="fa fa-question-circle" style="color:seagreen"></i>&nbsp;&nbsp;
> What if the name is not on the list? For instance, I want to create a
> person class, which has a getter and a setter for a name and age.<br><br>
> In this case, use the name of the function prepended with the **get** or **set**.
> For example:
> ```cpp
> void set_name(std::string name);
> std::string get_name();
> int16_t get_age();
> void set_age(int16_t age);
> ```

> <i class="fa fa-question-circle" style="color:seagreen"></i>&nbsp;&nbsp;
> What if I am dealing with a class that has multiple members that use
> a common method, such as erase or find?<br><br>
> In this case, use the member as the suffix for the method.
```cpp
 int16_t find_id( std::string name );
 int16_t find_address( in16_t id );
```

> <i class="fa fa-exclamation-triangle" style="color:darkkhaki"></i>&nbsp;&nbsp;
> Even if you only implement a getter or setter for piece of data in 
> a class, you should still preprend the *get_* or *set_*.
### Examples:

```cpp
class CamelCase
{
  public:
    // Constructors
      CamelCase() = default;
    // Public Methods
      // Element Access
      int find();
      // Modifiers
      void insert();
      void erase();
    // Operators
      friend std::ostream& operator<<(ostream& os, CamelCase rhs);
}
```

## Comment your class sections

Visual aid is always nice to quickly find what you are looking for.

### Example:
```cpp
template<typename T>
using Name = std::unique_ptr<T>;
template<typename T>
using Age = std::unique_ptr<T>;

template<typename T1, typename T2>
class Person
{
  private:
  // Private Members
    Name<T1> name;
    Age<T2> age;
  public:
  // Constructors
    template<typename U1, typename U2>
    Person(U1&& name, U2&& age);
  // Public Methods
    // Element Access
    T1 const& get_name();
    T2 const& get_age();
  // Operators
    template<typename _T1, typename _T2>
    friend std::ostream& operator<<(std::ostream& os, Person<_T1,_T2> const& p);
};
```

> <i class="fa fa-exclamation-triangle" style="color:darkkhaki"></i>&nbsp;&nbsp;
> There is a model to start with in the
> [resources section](https://formigoni.gitlab.io/anubis/resources-class.html).

> <i class="fa fa-exclamation-triangle" style="color:darkkhaki"></i>&nbsp;&nbsp;
> Inside the class and in the methods definitions, do not include
> empty commented sections, you can always consult offered model.

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> Notice how the commented sections inside the class, are on the same level
as the access specifiers.

> <i class="fa fa-info-circle" style="color:steelblue"></i>&nbsp;&nbsp;
> Notice how the member types are nested on the same level as the method, i.e.,
> *// Element Access*, **do not** include these in the definitions of
> your methods.
