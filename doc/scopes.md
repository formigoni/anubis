# Scopes

**Scope** Delimiters **{** and **}** should be below the target keywords.

### Example:

```cpp
namespace anastasia
{
  // content
} // namespace anastasia
```

**Argument List** delimiters **(** and **)**, **(** should be concatenated with
the keyword. 

### Example:

```cpp
if( my_bool == true )
{
  // do some logic
}
```

Every operator in the argument list has a single space between operands.

The initial and final operands should have a single space from the parenthesis.

### Example:

```cpp
if( my_bool == true && other_bool == false )
{
  // do some logic
}

```
