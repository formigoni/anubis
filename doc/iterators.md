# Iterators

**Iterators** should be written as follows:

* For input iterators, use:
```cpp
template<typename InIt>
```

* For output iterators, use:
```cpp
template<typename OutIt>
```

* For forward iterators, use:
```cpp
template<typename FwdIt>
```

* For bidirectional iterators, use:
```cpp
template<typename BidIt>
```

* For random access iterators, use:
```cpp
template<typename RandIt>
```
